import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// import AppNavbar from './AppNavbar';

//Import Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';

//createRoot - assigns the element to be managed by React with its Virtual DOM
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App/>
  </React.StrictMode>
);

//render() - displays the react elements/components into the root.
//App component is our mother component, this is the component we use as entry point and where we can render all other components or pages.
//<React.StrictMode> - component from React that manages future or possible conflicts. It allows us to extend or expand certain error messages.


/*const name = "Cardo Dalisay";
const student = {
  firstName: "Peter",
  lastName: 'Parker'
}

function userName(user){

  return user.firstName + ' ' + user.lastName

}

const element = <h1> Hello, {userName(student)} and {name}</h1>
root.render(element);*/