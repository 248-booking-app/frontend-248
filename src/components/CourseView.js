import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function CourseView() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate()

	const {courseId} = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const enroll = (courseId) =>{
		fetch(`http://localhost:4000/users/enroll`,{
			method: "POST",
			headers: {
				"Content-Type":"application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)
			if(data === true){
				Swal.fire({
					title: "Successfully enrolled",
					icon:"success",
					text:"ENROLLED!"
				})

				navigate("/courses")
			}else{
				Swal.fire({
					title: "Error",
					icon:"error",
					text:"ERROR!"
				})
			}
		})
	}
	

	useEffect(()=>{
		console.log(courseId);

		fetch(`http://localhost:4000/courses/${courseId}`)
		.then(res=>res.json())
		.then(data=>{
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})


	},[courseId]);




	


	

	return (
	<Container className="mt-5">
		<Row className="mt-3 mb-3">
	        <Col xs={12} md={12}>
	            <Card className = "cardHighlight p-3">
	              <Card.Body>
	                <Card.Title>{name}</Card.Title>
	                <Card.Subtitle>Description</Card.Subtitle>
	                <Card.Text>
	                  {description}
	                </Card.Text>
	                <Card.Subtitle>Price</Card.Subtitle>
	                <Card.Text>
	                  Php {price}
	                </Card.Text>
	                <Card.Subtitle>
	                  Class Schedule:
	                </Card.Subtitle>
	                <Card.Text>
	                 8:00AM - 5:00PM
	                </Card.Text>

	                {
	                	(user._id !== null)?
	                	<Button className="bg-primary" onClick={()=>enroll(courseId)}>Enroll</Button>
	                	:
						<Button className="bg-danger" as={Link} to="/login">Login to Enroll</Button>
	                }

	                
	              </Card.Body>
	            </Card>
	        </Col>
	    </Row>
	</Container>

	)


}