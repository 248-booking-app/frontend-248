// import { useState, useEffect } from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

//useState hook:
  //A hook in React is a kind of tool. The useState hook allows creation and manipulation of states

  //States are a way for React to keep track of any value and associate it with a component

  //When a state changes, React re-renders ONLY the specific component or part of the component that changed (and not the entire page or components whose states have not changed)

  //syntax:
  //const [state, setState] = useState(default state)

  //state: a special kind of variable (can be named anything) that React uses to render/re-render components when needed

  //state setter: State setters are the ONLY way to change a state's value. By convention, they are named after the state.

  //default state: The state's initial value on component mount
  //Before a component mounts, a state actually defaults to undefined, THEN is changed to its default state

  //array destructuring to get the state and the setter

  // Passing an object from Parent(Courses) to Child(CourseCard)

export default function CourseCard({courseProp}) {

  // console.log(courseProp);
  // console.log(typeof courseProp);
  // console.log(courseProp.name)

  // const [count, setCount] = useState(0);
  // const [seats, setSeats] = useState(10);
  //console.log(useState(0))

  // function enroll(){
  //   if(count<10){
  //     setCount(count + 1)
  //     setSeats(seats - 1)
  //   }
  //   /*else{
  //     alert("Maximum Enrollees Reached!")
  //   }*/
  // }

  // useEffect(()=>{
  //   if (seats === 0){
  //     alert("No more seats are available!")
  //   }
  // })

  const {name, description, price, _id} = courseProp;

  return (
    <Row className="mt-3 mb-3">
        <Col xs={12} md={12}>
            <Card className = "cardHighlight p-3">
              <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description</Card.Subtitle>
                <Card.Text>
                  {description}
                </Card.Text>
                <Card.Subtitle>Price</Card.Subtitle>
                <Card.Text>
                  {price}
                </Card.Text>
                <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
              </Card.Body>
            </Card>
        </Col>
    </Row>
  )
}