/*
  BrowserRouter is a component provided by the React Router library that enables client-side routing in a React application. It uses HTML5 history API to keep the UI in sync with the URL, allowing you to build a single-page application with multiple views or pages that can be navigated without refreshing the browser.
*/

//import {Fragment} from 'react';

import { useState, useEffect } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';

import {Container} from 'react-bootstrap';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import CourseView from './components/CourseView';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import './App.css';

//import UserProvider from UserContext
import { UserProvider } from './UserContext';

function App() {


  //this will be used to store user information and will be used for validating if a user is logged in on the app or not

  //a state hook for the user state that's defined here for a global scope
  //initialized as an object with properties from the localStorage
  //this will be used to store the user information and will be used for validating if a user is logged in on the app or not

  //1. This time we will create a state that will be used all throughout our components in our application

 // const [user, setUser] = useState({email: localStorage.getItem('email')});
  const [user, setUser] = useState({
    id:null,
    isAdmin:null
  });


  //function for clearing localStorage upon logging out
  //2. Define useState hook the create unsetUser function for clearing the local storage upon logout
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(()=>{
    fetch(()=>{
      fetch('http://localhost:4000/users/details',{
        headers:{
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(res=>res.json())
      .then(data=>{

        console.log(data);

        if(typeof data._id !== "undefined"){

          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        }else{
          setUser({
            id: null,
            isAdmin: null
          })
        }
      })
    })
  },[])



  //wrap all the components within the UserProvider to allow re-rendering when the "value" prop changes

  return (

    <UserProvider value={{user,setUser,unsetUser}}>

          <Router>
            <AppNavbar/>
              <Container>
                  <Routes>
                    <Route path="/" element={<Home/>}/>
                    <Route path="/courses" element={<Courses/>}/>
                    <Route path="/courses/:courseId" element={<CourseView/>}/>
                    <Route path="/login" element={<Login/>}/>
                    <Route path="/logout" element={<Logout/>}/>
                    <Route path="/register" element={<Register/>}/>
                    <Route path="*" element={<Error/>}/>
                  </Routes>
              </Container>
          </Router>

    </UserProvider>
  );
}

export default App;
