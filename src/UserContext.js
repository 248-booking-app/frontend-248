//Use React's Context API to give the authenticated user object to have "global" scope within our application

import React from 'react';

//Creation of Context Object
//a context object as the name states is a data type of an object that can be used to store information that can be shared to other components within the app

//context object is a different approach to passing information between components and allows easier access by avoiding the use of prop-drilling

const UserContext = React.createContext();
/*
	A context object is created using the React.createContext() method, which returns an object with two properties: Provider and Consumer. The Provider component is used to wrap the components that need access to the context, and it provides the context value to all of its descendants. The Consumer component is used to access the context value within a component.

*/

//The "Provider" component allows other components to consume/use the context object and supply the necessary information needed
export const UserProvider = UserContext.Provider;

export default UserContext;