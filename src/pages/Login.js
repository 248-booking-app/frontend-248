//consume the UserContext object in the login page using "useContext" hook to store the user details for future use

//import the "useContext" hook and the "UserContext" object. Deconstruct the user state and setUser setter function from the "UserContext" object
import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'

export default function Login() {

	//allows us to consume the User context object and its properties to use for user validation

	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isActive, setIsActive] = useState(false);


	function authenticate(e){



		e.preventDefault();

		fetch('http://localhost:4000/users/login',{
			method:'POST',
			headers:{
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);

			if(typeof data.access !== "undefined"){
				localStorage.setItem('token',data.access);
				retrieveUserDetails(data.access)

				Swal.fire({
					title:"Login Successful",
					icon: "success",
					text:"Welcome to Zuitt!"
				})
			}else{
				Swal.fire({
					title:"Authentication Failed",
					icon:"error",
					text:"Please, check your login details and try again!"
				})
			}
		});

		/*
			Local storage is a feature in web browsers that allows web applications to store data locally on the user's computer. This data persists even after the user closes the browser or navigates away from the web page.

			Local storage provides a simple key-value store, where you can save and retrieve data by specifying a key. The data is stored as a string, so you need to convert it to the appropriate data type when you retrieve it.

		*/

		/*
			
			The main difference between local storage and session storage in web browsers is the scope of the storage.

			Local storage and session storage both provide a way to store data on the client side, but they differ in terms of how long the data persists and in what circumstances it can be accessed.

			Here are the key differences between local storage and session storage:

			Persistence: Local storage data persists even after the user closes the browser, while session storage data is cleared when the user closes the browser or navigates away from the web page.

			Scope: Local storage data is shared across all tabs and windows of the same origin (i.e., same domain, protocol, and port), while session storage data is only accessible within the same tab or window that created it.

			Usage: Local storage is typically used for storing long-term data, such as user preferences, while session storage is typically used for storing short-term data, such as a user's login status or shopping cart contents.




		*/

		//Set the email of the authenticated user in the localStorage
		/*
			
			Syntax:
				localStorage.setItem("propertyName", value)
		*/

		//localStorage.setItem("email",email);

		//set the global user state to have properties obtained from localStorage

		//setUser({email: localStorage.getItem('email')});

		setEmail("");
		setPassword("");

		//alert(`Welcome back, ${email}!`)

	}

	const retrieveUserDetails = (token)=>{
		fetch('http://localhost:4000/users/details',{
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);
			setUser({
				id:data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(()=>{

		if(email !== "" && password !== ""){
			setIsActive(true)
		}else{
			setIsActive(false)
		}

	},[email, password])


	return (

	(user._id !== null)?
		<Navigate to="/courses"/>
		:
		<>
			<h1>Login</h1>

			<Form onSubmit={(e)=>authenticate(e)}>

		      <Form.Group className="mb-3" controlId="userEmail">
		        <Form.Label>Email Address</Form.Label>
		        <Form.Control
		        	type="email"
		        	placeholder="Enter Email"
		        	value = {email}
		        	onChange = {e=>setEmail(e.target.value)}
		        	required/>
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password">
		        <Form.Label>Password</Form.Label>
		        <Form.Control
		        	type="password"
		        	placeholder="Password"
		        	value = {password}
		        	onChange = {e=>setPassword(e.target.value)}
		        	required/>
		      </Form.Group>

		      {isActive ?

		      	<Button variant="success" type="submit" id="submitBtn">
		       		Submit
		      	</Button>

		      	:

		      	<Button variant="secondary" type="submit" id="submitBtn" disabled>
		        	Submit
		      	</Button>

		      }

		    </Form>
		 </>
	)
}