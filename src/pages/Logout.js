import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout() {

	//consume the UserContext object and destruicture it to access the user state and the unsetUser function from the context provider

	const { unsetUser, setUser} = useContext(UserContext);

	unsetUser();

	useEffect(()=>{

		setUser({_id:null})

	})

	return (

		//redirect/navigate back to login
		<Navigate to ="/"/>

	)



}