import { useEffect, useState } from 'react';
import CourseCard from '../components/CourseCard'
// import courses from '../data/courses';

export default function Courses() {

	const [courses, setCourses] = useState([]);

	useEffect(()=>{

		fetch(`http://localhost:4000/courses/`)
		.then(res=>res.json())
		.then(data => {

			// console.log(data);


			const coursesMap = (data.map(course=>{
				return(
					<CourseCard key={course._id} courseProp={course}/>
				)
			}))

			setCourses(coursesMap)


		})



	})

	// console.log(courses);
	// console.log(courses[0]);

	// const coursesMap = courses.map(course=>{

	// 	return(
	// 		//In React, keys are a special attribute that can be used to help React identify which items in a list have changed, been added, or been removed. When rendering a list of items in React, each item should have a unique key assigned to it.

	// 		//Keys should be added as a special attribute to each child element of a list. The value of the key should be a unique identifier that can be used by React to efficiently update the DOM when changes occur.
			
	// 		//So in summary, keys are a special attribute in React that are used to help identify items in a list and efficiently update the DOM when changes occur. They should be added as a unique attribute to each child element of a list.
	// 		<CourseCard key={course.id} courseProp={course}/>

	// 	)


	// })


	return(
		<>		
			<h1>Courses</h1>
			{courses}
		</>
	)



}