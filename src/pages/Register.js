//Import useState from react and define useState hooks
//Import useEffect

import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext'

export default function Register(){

	const { user } = useContext(UserContext);

	//4. Define state hooks for all input fields and add an "isActive" state for conditional rendering of the submit button

	const [firstName,setFirstName] = useState("");
	const [lastName,setLastName] = useState("");
	const [email,setEmail] = useState("");
	const [mobileNo,setMobileNo] = useState("");
	const [password, setPassword] = useState("");
	const [password2, setPassword2] = useState("");
	//state to determine whether the submit button is enabled or not
	const [isActive, setIsActive] = useState(false);



	console.log(firstName);
	console.log(lastName);
	console.log(email);
	console.log(mobileNo);
	console.log(password);
	console.log(password2);

	//Function to simulate user registration

	function registerUser(e){

		//prevents page redirection via form submission
		e.preventDefault();
		//clear input fields

		setFirstName("");
		setLastName("");
		setEmail("");
		setMobileNo("");
		setPassword("");
		setPassword2("");

		alert("Thank you for signing up!")
	}


	//Define a useEffect for validating user input

	useEffect(()=>{
		//validation to enable the submit button when all fields are populated and both passwords match (side effect)

		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo.length === 11 && password !== "" && password2 !=="") && (password === password2)){
			setIsActive(true)
		}else{
			setIsActive(false)
		}

	},[firstName, lastName, email, mobileNo, password, password2])

	//isActive state is for conditional rendering

	return (


		(user._id !== null)?
		<Navigate to="/courses"/>
		:
		<>
		<h1>Register</h1>
		{/*Bind the input states via 2-way binding.
			In React, two-way binding refers to the ability to bind the state of a component to the value of an input element, so that changes in either the state or the input value are automatically reflected in both.

			This means that when a user types into an input field, the component's state is updated to reflect the new value. Conversely, if the component's state is updated through other means (e.g., a database update or a user action), the input field's value is updated to reflect the new state.

			Two-way binding in React is commonly achieved using controlled components. A controlled component is a form element whose value is controlled by React through state. In other words, the value of the form element is always derived from the state of the component.
		*/}



		{/*
			In React, {e=>setFirstName(e.target.value)} is an arrow function that is typically used as an event handler for a form input element.

			The function takes an event object (e) as its parameter and calls the setFirstName function with the value of the input field.

			In this example, the onChange event is triggered whenever the value of the input field changes. When this happens, the arrow function is called with the event object as its parameter. The arrow function then extracts the current value of the input field using e.target.value and passes it to the setFirstName function.

			setFirstName is a state setter function provided by React's useState hook, calling setFirstName with a new value will update the state of the component with the new first name value.

		*/}
		<Form onSubmit={(e)=>registerUser(e)}>
			<Form.Group className="mb-3" controlId="firstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter First Name"
					value = {firstName}
					onChange = {e=>setFirstName(e.target.value)}
					required/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="lastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter Last Name"
					value={lastName}
					onChange={e=>setLastName(e.target.value)}
					required/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="userEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e=>setEmail(e.target.value)}
					required/>
				<Form.Text className="text-muted">
				We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group className="mb-3" controlId="mobileNo">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter Mobile Number"
					value={mobileNo}
					onChange={e=>setMobileNo(e.target.value)}
					required/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password}
					onChange={e=>setPassword(e.target.value)}
					required/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="password2">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify Password"
					value={password2}
					onChange={e=>setPassword2(e.target.value)}
					required/>
			</Form.Group>
			
			{isActive ?
				<Button variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>
				:
				<Button variant="primary" type="submit" id="submitBtn" disabled>
					Submit
				</Button>
			}
		</Form>
		</>
	)
}

